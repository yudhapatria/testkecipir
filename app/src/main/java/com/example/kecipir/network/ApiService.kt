package com.example.kecipir.network

import com.example.kecipir.db.model.BannerResponse
import com.example.kecipir.db.model.CategoryResponse
import com.example.kecipir.db.model.PageResponse
import com.example.kecipir.db.model.ProductsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("banner.json")
    suspend fun getBanner(): Response<BannerResponse>

    @GET("category.json")
    suspend fun getCategory(): Response<CategoryResponse>

    @GET("page{id}.json")
    suspend fun getProduk(@Path("id") id: Int?): Response<PageResponse>

    @GET("products.json")
    suspend fun getAllProduk(): Response<ProductsResponse>

}