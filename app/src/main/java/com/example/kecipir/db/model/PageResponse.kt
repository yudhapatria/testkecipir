package com.example.kecipir.db.model

import com.google.gson.annotations.SerializedName

data class PageResponse(@SerializedName("code") val code : Int,
                        @SerializedName("message") val message : String,
                        @SerializedName("data") val data : List<DataPage>)

data class DataPage (

    @SerializedName("id_product") val id_product : Int,
    @SerializedName("id_harvest") val id_harvest : Int,
    @SerializedName("farmer_discount") val farmer_discount : Int,
    @SerializedName("kecipir_discount") val kecipir_discount : Int,
    @SerializedName("id_product_farmer") val id_product_farmer : Int,
    @SerializedName("id_farmer_product") val id_farmer_product : Int,
    @SerializedName("id_category") val id_category : String,
    @SerializedName("id_grade") val id_grade : String,
    @SerializedName("farmer") val farmer : String,
    @SerializedName("exp_day") val exp_day : Int,
    @SerializedName("harvest_date") val harvest_date : String,
    @SerializedName("title") val title : String,
    @SerializedName("title_en") val title_en : String,
    @SerializedName("code") val code : String,
    @SerializedName("share_link") val share_link : String,
    @SerializedName("photo") val photo : String,
    @SerializedName("id_farmer") val id_farmer : Int,
    @SerializedName("buy_price") val buy_price : Int,
    @SerializedName("sell_price") val sell_price : Int,
    @SerializedName("promo_price") val promo_price : Int,
    @SerializedName("sell_price_text") val sell_price_text : String,
    @SerializedName("sell_price_color") val sell_price_color : String,
    @SerializedName("discount") val discount : String,
    @SerializedName("unit") val unit : String,
    @SerializedName("grade") val grade : String,
    @SerializedName("grade_note") val grade_note : String,
    @SerializedName("grade_color") val grade_color : String,
    @SerializedName("is_stock") val is_stock : Int,
    @SerializedName("stock_active") val stock_active : List<Stock_active>,
    @SerializedName("stock") val stock : Int,
    @SerializedName("qty_cart") val qty_cart : Int,
    @SerializedName("stock_text") val stock_text : Int,
    @SerializedName("stock_color") val stock_color : String,
    @SerializedName("rating") val rating : Int,
    @SerializedName("social_buying") val social_buying : Boolean
)


data class Stock_active (

    @SerializedName("id_harvest") val id_harvest : Int,
    @SerializedName("id_petani_barang") val id_petani_barang : Int,
    @SerializedName("item_id") val item_id : Int,
    @SerializedName("nama") val nama : String,
    @SerializedName("barang") val barang : String,
    @SerializedName("tgl_panen") val tgl_panen : String,
    @SerializedName("qty_awal") val qty_awal : Int,
    @SerializedName("qty") val qty : Int,
    @SerializedName("exp_day") val exp_day : Int,
    @SerializedName("diff") val diff : Int,
    @SerializedName("stock_available") val stock_available : Int
)
