package com.example.kecipir.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.kecipir.db.model.DataProducts
import com.example.kecipir.db.model.ProductsResponse

@Dao
interface KeranjangPembelianPopulerDao {

    @Query("SELECT * FROM KeranjangPembelian where id_harvest == :id")
    fun isProductExist(id: Int): Boolean

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(produk: DataProducts)

    @Query("UPDATE KeranjangPembelian SET quantity = quantity + 1 WHERE id_harvest = :idHarvest")
    fun updateQuantityPlus(idHarvest: Int)

    @Query("UPDATE KeranjangPembelian SET quantity = quantity - 1 WHERE id_harvest = :idHarvest")
    fun updateQuantityMinus(idHarvest: Int)

    @Query("SELECT * FROM KeranjangPembelian")
    fun findAllProduct(): List<DataProducts>

    @Query("DELETE FROM KeranjangPembelian WHERE id_harvest = :idHarvest")
    fun deleteProduct(idHarvest: Int)

}