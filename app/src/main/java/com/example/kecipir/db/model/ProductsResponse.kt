package com.example.kecipir.db.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class ProductsResponse (
    @SerializedName("code") val code : Int,
    @SerializedName("message") val message : String,
    @SerializedName("data") val data : List<DataProducts>
        ): Parcelable


@Entity(tableName = "KeranjangPembelian")
@Parcelize
data class DataProducts (
    @PrimaryKey(autoGenerate = true) val iddb: Int,
    @SerializedName("id_harvest") val id_harvest : Int,
    @SerializedName("id_product_farmer") val id_product_farmer : Int,
    @SerializedName("farmer") val farmer : String,
    @SerializedName("harvest_date") val harvest_date : String,
    @SerializedName("title") val title : String,
    @SerializedName("title_en") val title_en : String,
    @SerializedName("code") val code : String,
    @SerializedName("share_link") val share_link : String,
    @SerializedName("photo") val photo : String,
    @SerializedName("sell_price") val sell_price : Int,
    @SerializedName("promo_price") val promo_price : Int,
    @SerializedName("discount") val discount : String,
    @SerializedName("unit") val unit : String,
    @SerializedName("grade") val grade : String,
    @SerializedName("grade_note") val grade_note : String,
    @SerializedName("grade_color") val grade_color : String,
    @SerializedName("stock") val stock : Int,
    @SerializedName("qty_cart") val qty_cart : Int,
    @SerializedName("rating") val rating : Int,
    @SerializedName("quantity") var quantity : Int
): Parcelable