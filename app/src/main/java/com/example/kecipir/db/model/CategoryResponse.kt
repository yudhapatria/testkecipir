package com.example.kecipir.db.model

import com.google.gson.annotations.SerializedName

data class CategoryResponse(
    @SerializedName("code") val code : Int,
    @SerializedName("message") val message : String,
    @SerializedName("data") val data : List<DataCategory>
)

data class DataCategory (

    @SerializedName("id") val id : String,
    @SerializedName("title") val title : String,
    @SerializedName("category") val category : String,
    @SerializedName("link") val link : String,
    @SerializedName("meta_description") val meta_description : String,
    @SerializedName("block") val block : String,
    @SerializedName("countdown") val countdown : Int,
    @SerializedName("poster") val poster : String,
    @SerializedName("item") val item : Int,
    @SerializedName("ordering") val ordering : Int
)
