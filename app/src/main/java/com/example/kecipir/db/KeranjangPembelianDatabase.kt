package com.example.kecipir.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.kecipir.db.model.DataProducts

@Database(
    entities = [DataProducts::class],
    version = 2, exportSchema = false
)
abstract class KeranjangPembelianDatabase : RoomDatabase() {
    abstract val keranjangPembelianPopulerDao : KeranjangPembelianPopulerDao
}