package com.example.kecipir.db.model

import com.google.gson.annotations.SerializedName

data class BannerResponse(
    @SerializedName("code") val code : Int,
    @SerializedName("message") val message : String,
    @SerializedName("data") val data : List<DataBanner>
)

data class DataBanner (

    @SerializedName("id") val id : Int,
    @SerializedName("judul") val judul : String,
    @SerializedName("url") val url : String,
    @SerializedName("image") val image : String,
    @SerializedName("type") val type : String,
    @SerializedName("param") val param : String
)
