package com.example.kecipir.di

import com.example.kecipir.ui.detailProduk.DetailProdukViewModel
import com.example.kecipir.ui.home.HomeViewModel
import com.example.kecipir.ui.searchProduk.SearchProdukViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        HomeViewModel(get())
    }

    viewModel {
        SearchProdukViewModel(get())
    }

    viewModel {
        DetailProdukViewModel(get())
    }
}