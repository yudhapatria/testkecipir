package com.example.kecipir.di

import android.content.Context
import com.example.kecipir.network.ApiService
import com.example.kecipir.repository.HomeRepository
import com.example.kecipir.repository.HomeRepositoryImp
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {

    fun provideBannerRepository(api: ApiService, context: Context): HomeRepository {
        return HomeRepositoryImp(api, context)
    }
    single { provideBannerRepository(get(), androidContext()) }

}