package com.example.kecipir.di

import android.app.Application
import androidx.room.Room
import com.example.kecipir.db.KeranjangPembelianDatabase
import com.example.kecipir.db.KeranjangPembelianPopulerDao
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {

    fun provideDatabase(application: Application): KeranjangPembelianDatabase {
        return Room.databaseBuilder(application, KeranjangPembelianDatabase::class.java, "KeranjangPembelian")
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideMoviesDao(database: KeranjangPembelianDatabase): KeranjangPembelianPopulerDao {
        return database.keranjangPembelianPopulerDao
    }

    single { provideDatabase(androidApplication()) }
    single { provideMoviesDao(get()) }

}