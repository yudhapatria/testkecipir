package com.example.kecipir.di

import com.example.kecipir.network.ApiService
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule  = module {

    fun provideKecipirApi(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
    single { provideKecipirApi(get()) }

}