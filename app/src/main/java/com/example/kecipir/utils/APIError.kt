package com.example.kecipir.utils

data class APIError(val message: String) {
    constructor() : this("")
}
