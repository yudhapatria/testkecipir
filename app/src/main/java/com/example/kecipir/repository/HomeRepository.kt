package com.example.kecipir.repository

import com.example.kecipir.db.model.BannerResponse
import com.example.kecipir.db.model.CategoryResponse
import com.example.kecipir.db.model.PageResponse
import com.example.kecipir.db.model.ProductsResponse
import com.example.kecipir.utils.AppResult

interface HomeRepository {

    suspend fun getBanner(): AppResult<BannerResponse>
    suspend fun getCategory(): AppResult<CategoryResponse>
    suspend fun getProduct(id: Int): AppResult<PageResponse>
    suspend fun getAllProduct(): AppResult<ProductsResponse>
}