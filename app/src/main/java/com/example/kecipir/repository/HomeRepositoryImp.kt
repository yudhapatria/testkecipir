package com.example.kecipir.repository

import android.content.Context
import android.util.Log
import com.example.kecipir.db.model.BannerResponse
import com.example.kecipir.db.model.CategoryResponse
import com.example.kecipir.db.model.PageResponse
import com.example.kecipir.db.model.ProductsResponse
import com.example.kecipir.network.ApiService
import com.example.kecipir.utils.AppResult
import com.example.kecipir.utils.NetworkManager.isOnline
import com.example.kecipir.utils.Utils.handleApiError
import com.example.kecipir.utils.Utils.handleSuccess
import com.example.kecipir.utils.noNetworkConnectivityError
import java.lang.Exception

class HomeRepositoryImp(private val apiService: ApiService,
                        private val context: Context):HomeRepository {

    override suspend fun getBanner(): AppResult<BannerResponse> {
        val response = apiService.getBanner()

        return try{
            if(response.isSuccessful){
                handleSuccess(response)

            }else {
                if(isOnline(context)){
                    handleApiError(response)
                }else{
                    context.noNetworkConnectivityError()
                }
            }
        }catch (e: Exception){
            AppResult.Error(e)
        }
    }

    override suspend fun getCategory(): AppResult<CategoryResponse> {
        val response = apiService.getCategory()

        return try{
            if(response.isSuccessful){
                handleSuccess(response)

            }else {
                if(isOnline(context)){
                    handleApiError(response)
                }else{
                    context.noNetworkConnectivityError()
                }
            }
        }catch (e: Exception){
            AppResult.Error(e)
        }
    }

    override suspend fun getProduct(id : Int): AppResult<PageResponse> {
        try {


        val response = apiService.getProduk(id)
        return try{
            if(response.isSuccessful){
                handleSuccess(response)

            }else {
                if(isOnline(context)){
                    handleApiError(response)
                }else{
                    context.noNetworkConnectivityError()
                }
            }
        }catch (e: Exception){
            AppResult.Error(e)
        }
        }catch (e: Exception){
            Log.d("opoiko", e.toString())
            return AppResult.Error(e)
        }
    }

    override suspend fun getAllProduct(): AppResult<ProductsResponse> {

        try {

            val response = apiService.getAllProduk()
            return try{
                if(response.isSuccessful){
                    handleSuccess(response)

                }else {
                    if(isOnline(context)){
                        handleApiError(response)
                    }else{
                        context.noNetworkConnectivityError()
                    }
                }
            }catch (e: Exception){
                AppResult.Error(e)
            }
        }catch (e: Exception){
            Log.d("opoiko", e.toString())
            return AppResult.Error(e)
        }
    }

}