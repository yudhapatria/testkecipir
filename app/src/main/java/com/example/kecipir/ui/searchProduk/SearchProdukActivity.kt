package com.example.kecipir.ui.searchProduk

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.kecipir.R
import com.example.kecipir.db.model.DataProducts
import com.example.kecipir.ui.home.adapter.PromoAdapter
import com.example.kecipir.ui.keranjang.KeranjangActivity
import com.example.kecipir.ui.searchProduk.adapter.SearchAdapter
import com.example.kecipir.utils.NetworkManager
import com.example.kecipir.utils.noNetworkConnectivityError
import kotlinx.android.synthetic.main.activity_detail_produk.*
import kotlinx.android.synthetic.main.activity_search_produk.*
import kotlinx.android.synthetic.main.activity_search_produk.img_back
import kotlinx.android.synthetic.main.activity_search_produk.img_shopping_basket
import org.koin.android.viewmodel.ext.android.viewModel

class SearchProdukActivity : AppCompatActivity() {

    private val searchProdukViewModel by viewModel<SearchProdukViewModel>()
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var searchAdapter: SearchAdapter
    private lateinit var promoAdapter: PromoAdapter
    private var dataAllProducts: ArrayList<DataProducts> = ArrayList()
    private var checkIntent: Int = 0
    private var idFilter : Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_produk)

        checkIntent = intent.getIntExtra("produk",0)




        gridLayoutManager = GridLayoutManager(this, 2)
        searchAdapter = SearchAdapter(this, dataAllProducts)
        promoAdapter = PromoAdapter(this, dataAllProducts)
        rv_allProduct.layoutManager = gridLayoutManager
        rv_allProduct.adapter = searchAdapter


        searchProdukViewModel.produkallList.observe(this, Observer { response->
            if(response!=null){

                if(checkIntent != 0){
                    rv_allProduct.visibility = VISIBLE
                    img_search.visibility = VISIBLE
                    cl_searchSayur.visibility = GONE
                    tv_title_search.visibility = VISIBLE
                    ll_filter.visibility = VISIBLE
                    if(checkIntent == 2){
                        rv_allProduct.adapter = promoAdapter
                        tv_title_search.text = "Promo"
                        for(items in response.data){
                            if(items.discount != "0%"){
                                dataAllProducts.add(items)
                            }
                        }
                    }else{
                        tv_title_search.text = "Semua Produk"
                        for(items in response.data){
                            if(items.discount == "0%"){
                                dataAllProducts.add(items)
                            }
                        }
                    }
                }else{
                    dataAllProducts.addAll(response.data)
                }
                //Toast.makeText(this, "masuk", Toast.LENGTH_LONG).show()
                searchAdapter.notifyDataSetChanged()
                promoAdapter.notifyDataSetChanged()
                when (idFilter) {
                    1 -> {
                        searchAdapter.filterOrganikBersertifikat()
                        promoAdapter.filterOrganikBersertifikat()
                        searchAdapter.notifyDataSetChanged()
                        promoAdapter.notifyDataSetChanged()
                    }
                    2 -> {
                        searchAdapter.filterPerlakuanOrganik()
                        promoAdapter.filterPerlakuanOrganik()
                    }
                    3 -> {
                        searchAdapter.filterSehat()
                        promoAdapter.filterSehat()
                    }
                    4 -> {
                        searchAdapter.filterHasilAlam()
                        promoAdapter.filterHasilAlam()
                    }
                    0 -> {
                      searchAdapter.filterSemua()
                        promoAdapter.filterSemua()
                    }
                }

                searchAdapter.notifyDataSetChanged()
                promoAdapter.notifyDataSetChanged()
                pg_produk_all.visibility = GONE


            }
        })

      getProduk()

        et_searchSayur.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                clear_search.visibility = VISIBLE
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        clear_search.setOnClickListener {
            et_searchSayur.text.clear()
        }


        et_searchSayur.setOnEditorActionListener { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                doSearch()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }

        img_search.setOnClickListener {
            img_search.visibility = GONE
            cl_searchSayur.visibility = VISIBLE
            tv_title_search.visibility = GONE
        }

        img_back.setOnClickListener {
            finish()
        }

        img_shopping_basket.setOnClickListener {
            startActivity(Intent(this, KeranjangActivity::class.java))
        }

        cl_urutkan.setOnClickListener {
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.custom_dialog_urutkan)
            val urutkanHargaTertinggi = dialog.findViewById(R.id.urutkan_harga_tertinggi) as TextView
            val urutkanHargaTerendah = dialog.findViewById(R.id.urutkan_harga_terendah) as TextView
            val urutkan_semua = dialog.findViewById<TextView>(R.id.urutkan_semua)


            urutkanHargaTertinggi.setOnClickListener {
                urutkanHargaTertinggi.setTextColor(resources.getColor(R.color.green_primary))
                urutkanHargaTerendah.setTextColor(resources.getColor(R.color.black))

                tv_urutkan.text = "Harga Tertinggi"
                searchAdapter.getSortHighPrice()
                promoAdapter.getSortHighPrice()
                dialog.dismiss()
            }

            urutkanHargaTerendah.setOnClickListener {
                urutkanHargaTerendah.setTextColor(resources.getColor(R.color.green_primary))
                urutkanHargaTertinggi.setTextColor(resources.getColor(R.color.black))
                tv_urutkan.text = "Harga Terendah"
                searchAdapter.getSortLowPrice()
                promoAdapter.getSortLowPrice()
                dialog.dismiss()
            }

            urutkan_semua.setOnClickListener {
                searchAdapter.getSortSemua(dataAllProducts)
                promoAdapter.getSortSemua()
                tv_urutkan.text = "Semua"
                dialog.dismiss()
            }

            dialog.show()
        }

        cl_grade.setOnClickListener {
            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.custom_dialog_filter)
            val filterBersertifikat = dialog.findViewById(R.id.filter_bersertifikat) as TextView
            val filterPerlakuanOrganik = dialog.findViewById<TextView>(R.id.filter_perlakuan_organik)
            val filter_sehat = dialog.findViewById<TextView>(R.id.filter_sehat)
            val filter_hasil_alam = dialog.findViewById<TextView>(R.id.filter_hasil_alam)
            val filter_semua = dialog.findViewById<TextView>(R.id.filter_semua)

            filterBersertifikat.setOnClickListener {
                idFilter = 1
                dataAllProducts.clear()
                searchAdapter.clearData()
                promoAdapter.clearData()
                tv_grade.text="Organik Bersertifikat"
                getProduk()
                dialog.dismiss()
            }

            filterPerlakuanOrganik.setOnClickListener {
                idFilter = 2
                dataAllProducts.clear()
                searchAdapter.clearData()
                promoAdapter.clearData()
                tv_grade.text="Perlakuan Organik"

                getProduk()

                dialog.dismiss()

            }

            filter_sehat.setOnClickListener {

                idFilter =3
                dataAllProducts.clear()
                searchAdapter.clearData()
                promoAdapter.clearData()
                tv_grade.text="Sehat"

                getProduk()
                dialog.dismiss()
            }

            filter_hasil_alam.setOnClickListener {
                idFilter =4
                dataAllProducts.clear()
                searchAdapter.clearData()
                promoAdapter.clearData()
                tv_grade.text="Hasil Alam"

                getProduk()
                dialog.dismiss()
            }

            filter_semua.setOnClickListener {
                idFilter = 0
                dataAllProducts.clear()
                searchAdapter.clearData()
                promoAdapter.clearData()
                tv_grade.text="Semua"

                getProduk()
                dialog.dismiss()
            }

            dialog.show()
        }
    }

    private fun getProduk() {
        if(NetworkManager.isOnline(this)) {
            searchProdukViewModel.getProduk()
        }else{
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show()

        }
    }


    private fun doSearch() {
       // Toast.makeText(this, et_searchSayur.text.toString(), Toast.LENGTH_SHORT).show()
        searchAdapter.getFilter()?.filter(et_searchSayur.text.toString())
        promoAdapter.getFilter()?.filter(et_searchSayur.text.toString())
        rv_allProduct.visibility = VISIBLE
        ll_filter.visibility = VISIBLE

    }



}