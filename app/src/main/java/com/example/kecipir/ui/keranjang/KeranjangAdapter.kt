package com.example.kecipir.ui.keranjang

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.kecipir.R
import com.example.kecipir.db.KeranjangPembelianDatabase
import com.example.kecipir.db.model.DataProducts

class KeranjangAdapter(val context: Context?, var dataProduk: ArrayList<DataProducts>) : RecyclerView.Adapter<KeranjangAdapter.ViewHolder>() {
    private var searchProducts: ArrayList<DataProducts> = dataProduk


    var onItemClick: ((pos: Int) -> Unit)? = null




    var  db = context?.let {
        Room.databaseBuilder(
            it,
            KeranjangPembelianDatabase::class.java, "KeranjangPembelian"
        ).allowMainThreadQueries().build()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KeranjangAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_keranjang,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: KeranjangAdapter.ViewHolder, position: Int) {
        val dataProduks = searchProducts[position]

        holder.imageProduk?.let { image->
            context?.let { context ->
                Glide.with(context)
                    .load(dataProduks.photo)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.ic_baseline_image_not_supported_24)
                    .into(image)
            }
        }

        holder.tvNama?.text = dataProduks.title
        holder.tv_harga_keranjang?.text = dataProduks.promo_price.toString()
        holder.tv_unit_keranjang?.text = dataProduks.unit.toString()
        holder.tvFarm?.text = dataProduks.farmer
        holder.tv_stok_detail?.text = "Stok " +  dataProduks.stock.toString()
        holder.iv_trash?.setOnClickListener {
            db?.keranjangPembelianPopulerDao?.deleteProduct(dataProduks.id_harvest)
            searchProducts.clear()
            onItemClick?.invoke(position)
            notifyDataSetChanged()
            Toast.makeText(context, "Barang Berhasil Dihapus", Toast.LENGTH_SHORT).show()


        }

        holder.btn_minus?.setOnClickListener {
            if(holder.tv_quantity?.text.toString() == "1"){
                db?.keranjangPembelianPopulerDao?.deleteProduct(dataProduks.id_harvest)
                searchProducts.clear()
                notifyDataSetChanged()
                Toast.makeText(context, "Barang Berhasil Dihapus", Toast.LENGTH_SHORT).show()
            }else {
                holder.tv_quantity?.text =
                    (Integer.valueOf(holder.tv_quantity?.text.toString()) - 1).toString()
                db?.keranjangPembelianPopulerDao?.updateQuantityMinus(dataProduks.id_harvest)
            }
            onItemClick?.invoke(position)

        }

        holder.btn_plus?.setOnClickListener {

            if(Integer.valueOf(holder.tv_quantity?.text.toString()) < dataProduks.stock) {
                holder.tv_quantity?.text =
                    (Integer.valueOf(holder.tv_quantity?.text.toString()) + 1).toString()
                db?.keranjangPembelianPopulerDao?.updateQuantityPlus(dataProduks.id_harvest)

            }else{
                Toast.makeText(context, "Tidak bisa menambah jumlah barang", Toast.LENGTH_SHORT).show()

            }
            onItemClick?.invoke(position)

        }


    }

    override fun getItemCount(): Int {
         return  searchProducts.size
    }

    fun getFilter(): Filter? {
        return articleFilter
    }

    private val articleFilter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults? {
            val filteredList: MutableList<DataProducts> = ArrayList()
            if (constraint == null || constraint.length == 0) {
                filteredList.addAll(dataProduk)
            } else {
                val filterPattern =
                    constraint.toString().toLowerCase().trim { it <= ' ' }
                for (item in dataProduk) {
                    if (item.title.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item)
                    }
                }
                if(filteredList.size ==0){
                    for (item in dataProduk) {
                        if (item.farmer.toLowerCase().contains(filterPattern)) {
                            filteredList.add(item)
                        }
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(
            constraint: CharSequence?,
            results: FilterResults
        ) {
            searchProducts = results.values as ArrayList<DataProducts>
            notifyDataSetChanged()
        }
    }

//    fun setData(article: List<DataProducts>){
//        searchProducts = article
//        notifyDataSetChanged()
//
//    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageProduk: ImageView? = itemView.findViewById(R.id.imgProduk)
        val tvNama: TextView? = itemView.findViewById(R.id.tv_nama_keranjang)
        val tv_harga_keranjang: TextView?= itemView.findViewById(R.id.tv_harga_keranjang)
        val tv_unit_keranjang: TextView? = itemView.findViewById(R.id.tv_unit_keranjang)
        val tvFarm: TextView? = itemView.findViewById(R.id.tvFarm)
        val tv_stok_detail: TextView? = itemView.findViewById(R.id.tv_stok_detail)
        val iv_trash : ImageView? = itemView.findViewById(R.id.iv_trash)
        val btn_plus: LinearLayout? = itemView.findViewById(R.id.btn_plus)
        val btn_minus: LinearLayout? = itemView.findViewById(R.id.btn_minus)
        val tv_quantity: TextView? = itemView.findViewById(R.id.tv_quantity)

    }
}