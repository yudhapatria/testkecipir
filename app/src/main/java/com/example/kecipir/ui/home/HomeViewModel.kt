package com.example.kecipir.ui.home

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.kecipir.db.model.BannerResponse
import com.example.kecipir.db.model.CategoryResponse
import com.example.kecipir.db.model.PageResponse
import com.example.kecipir.db.model.ProductsResponse
import com.example.kecipir.repository.HomeRepository
import com.example.kecipir.ui.baseViewModel.BaseViewModel
import com.example.kecipir.utils.AppResult
import com.example.kecipir.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class HomeViewModel constructor(private val repository: HomeRepository) : BaseViewModel() {

    val showLoading = ObservableBoolean()
    val bannerList = MutableLiveData<BannerResponse>()
    val showError= SingleLiveEvent<String>()
    val categoryList = MutableLiveData<CategoryResponse>()
    val produkList = MutableLiveData<PageResponse>()
    val produkPromoList = MutableLiveData<ProductsResponse>()

    fun getBanner(){
        showLoading.set(true)
        viewModelScope.launch {
            val result =repository.getBanner()
            showLoading.set(false)

            when(result){
                is AppResult.Success->{

                    result.successData?.let {
                        bannerList.value = it
                    }
                }
                is AppResult.Error -> showError.value = result.exception.message

            }
        }
    }

    fun getCategory(){
        showLoading.set(true)
        viewModelScope.launch {
            val result =repository.getCategory()
            showLoading.set(false)

            when(result){
                is AppResult.Success->{

                    result.successData?.let {
                        categoryList.value = it
                    }
                }
                is AppResult.Error -> showError.value = result.exception.message

            }
        }
    }

    fun getProduk(id : Int){
        showLoading.set(true)
        viewModelScope.launch {
            val result =repository.getProduct(id)
            showLoading.set(false)

            when(result){
                is AppResult.Success->{

                    result.successData?.let {
                        produkList.value = it
                    }
                }
                is AppResult.Error -> showError.value = result.exception.message

            }
        }
    }

    fun getProdukPromo(){
        showLoading.set(true)
        viewModelScope.launch {
            val result =repository.getAllProduct()
            showLoading.set(false)

            when(result){
                is AppResult.Success->{

                    result.successData?.let {
                        produkPromoList.value = it
                    }
                }
                is AppResult.Error -> showError.value = result.exception.message

            }
        }
    }
}