package com.example.kecipir.ui.home.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.kecipir.R
import com.example.kecipir.db.KeranjangPembelianDatabase
import com.example.kecipir.db.model.DataProducts
import com.example.kecipir.ui.detailProduk.DetailProdukActivity
import java.text.NumberFormat

class PromoAdapter(val context: Context?, var dataProduk: ArrayList<DataProducts>) : RecyclerView.Adapter<PromoAdapter.ViewHolder>() {
    private var searchProducts: ArrayList<DataProducts> = dataProduk
    var  db = context?.let {
        Room.databaseBuilder(
            it,
        KeranjangPembelianDatabase::class.java, "KeranjangPembelian"
    ).allowMainThreadQueries().build()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromoAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_produk, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: PromoAdapter.ViewHolder, position: Int) {
        val dataProduks = searchProducts[position]

        holder.imageProduk?.let { image->
            context?.let { context ->
                Glide.with(context)
                    .load(dataProduks.photo)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.ic_baseline_image_not_supported_24)
                    .into(image)
            }
        }

        holder.textFarmer?.text = dataProduks.farmer
        holder.tvNamaProduk?.text = dataProduks.title
        holder.tvHargaProduk?.text = "Rp. " + NumberFormat.getInstance().format(dataProduks.promo_price)
        holder.tvUnitProduk?.text = dataProduks.unit
        holder.tvGradeProduk?.text = dataProduks.grade
        holder.tvStokProduk?.text = "Stok " + dataProduks.stock.toString()
        holder.tvDiscount?.text = dataProduks.discount
        holder.tvHargaAsli?.text = "Rp. " + NumberFormat.getInstance().format(dataProduks.sell_price)
        holder.llPromo?.visibility = View.VISIBLE
        holder.tvHargaAsli?.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

        holder.clProduk?.setOnClickListener {
            val intent = Intent(context, DetailProdukActivity::class.java)



//            intent.putExtra("")
//            intent.putExtra("dataProdukPage", dataProdukIntent)
            context?.startActivity(intent)

        }

        holder.btnBeliProduk?.setOnClickListener {
         if(db?.keranjangPembelianPopulerDao?.isProductExist(dataProduks.id_harvest) == true){
             db?.keranjangPembelianPopulerDao?.updateQuantityPlus(dataProduks.id_harvest)
             Toast.makeText(context, "Berhasil Menambah Jumlah Barang", Toast.LENGTH_SHORT).show()


         }else{
             dataProduks.quantity = 1
            db?.keranjangPembelianPopulerDao?.add(dataProduks)
             Toast.makeText(context, "Berhasil Dimasukkan Keranjang", Toast.LENGTH_SHORT).show()

         }

        }

    }

    fun getSortLowPrice() {
        var sortProduct = searchProducts.sortedBy {
            it.sell_price
        }

        searchProducts.clear()
        searchProducts.addAll(sortProduct)
        notifyDataSetChanged()
    }

    fun getSortHighPrice() {
        var sortProduct = searchProducts.sortedBy {
            it.sell_price
        }
        searchProducts.clear()
        searchProducts.addAll(sortProduct)
        searchProducts.reverse()
        notifyDataSetChanged()
    }

    fun getSortSemua(){
        searchProducts.clear()
        searchProducts.addAll(dataProduk)
        Log.d("ikiyemek", searchProducts.size.toString())
        notifyDataSetChanged()
    }




    override fun getItemCount(): Int {
         return  searchProducts.size
    }

    fun clearData(){
        searchProducts.clear()
        dataProduk.clear()
    }

    fun filterOrganikBersertifikat(){
        var sortProduct =   dataProduk.filter {
            it.grade == "Organik Bersertifikat"
        }
        searchProducts.clear()
        searchProducts.addAll(sortProduct)
        notifyDataSetChanged()

    }

    fun filterPerlakuanOrganik(){

        var sortProduct =    dataProduk.filter {
            it.grade.equals("Perlakuan Organik ")
        }
        searchProducts.clear()
        searchProducts.addAll(sortProduct)
        notifyDataSetChanged()

    }

    fun filterSehat(){

        var sortProduct =   dataProduk.filter {
            it.grade.equals("Sehat")
        }
        searchProducts.clear()
        searchProducts.addAll(sortProduct)
        notifyDataSetChanged()

    }

    fun filterHasilAlam(){

        var sortProduct =  dataProduk.filter {
            it.grade == "Hasil Alam"
        }
        searchProducts.clear()
        searchProducts.addAll(sortProduct)
        notifyDataSetChanged()

    }

    fun filterSemua(){
        searchProducts = dataProduk
        Log.d("yoko", searchProducts.size.toString())
        notifyDataSetChanged()
    }

    fun getFilter(): Filter? {
        return produkFilter
    }

    private val produkFilter: Filter = object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults? {
            val filteredList: MutableList<DataProducts> = ArrayList()
            if (constraint == null || constraint.length == 0) {
                filteredList.addAll(dataProduk)
            } else {
                val filterPattern =
                    constraint.toString().toLowerCase().trim { it <= ' ' }
                for (item in dataProduk) {
                    if (item.title.toLowerCase().contains(filterPattern)) {
                        filteredList.add(item)
                    }
                }
                if(filteredList.size ==0){
                    for (item in dataProduk) {
                        if (item.farmer.toLowerCase().contains(filterPattern)) {
                            filteredList.add(item)
                        }
                    }
                }
            }
            val results = FilterResults()
            results.values = filteredList
            return results
        }

        override fun publishResults(
            constraint: CharSequence?,
            results: FilterResults
        ) {
            searchProducts = results.values as ArrayList<DataProducts>
            notifyDataSetChanged()
        }
    }

//    fun setData(article: List<DataProducts>){
//        searchProducts = article
//        notifyDataSetChanged()
//
//    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageProduk: ImageView? = itemView.findViewById(R.id.iv_produk)
        val textFarmer: TextView? = itemView.findViewById(R.id.tv_farmer)
        val tvNamaProduk: TextView? = itemView.findViewById(R.id.tv_nama_produk)
        val tvHargaProduk: TextView? = itemView.findViewById(R.id.tv_harga_produk)
        val tvUnitProduk: TextView? = itemView.findViewById(R.id.tv_unit_produk)
        val tvGradeProduk: TextView? = itemView.findViewById(R.id.tv_grade_produk)
        val tvStokProduk: TextView? = itemView.findViewById(R.id.tv_stok_produk)
        val tvDiscount: TextView? = itemView.findViewById(R.id.tv_discount)
        val tvHargaAsli: TextView? = itemView.findViewById(R.id.tv_harga_asli)
        val llPromo : LinearLayout? = itemView.findViewById(R.id.llPromo)
        val clProduk: ConstraintLayout? = itemView.findViewById(R.id.cl_produk)
        val btnBeliProduk: LinearLayout? = itemView.findViewById(R.id.btn_beli_produk)
    }
}