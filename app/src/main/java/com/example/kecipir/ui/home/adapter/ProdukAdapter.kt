package com.example.kecipir.ui.home.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.kecipir.R
import com.example.kecipir.db.KeranjangPembelianDatabase
import com.example.kecipir.db.model.DataCategory
import com.example.kecipir.db.model.DataPage
import com.example.kecipir.db.model.DataProducts
import com.example.kecipir.ui.detailProduk.DetailProdukActivity
import com.example.kecipir.ui.searchProduk.SearchProdukActivity
import org.w3c.dom.Text
import java.text.NumberFormat

class ProdukAdapter(val context: Context?, val dataProduk: ArrayList<DataProducts>) : RecyclerView.Adapter<ProdukAdapter.ViewHolder>() {
    var  db = context?.let {
        Room.databaseBuilder(
            it,
            KeranjangPembelianDatabase::class.java, "KeranjangPembelian"
        ).allowMainThreadQueries().build()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProdukAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_produk, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProdukAdapter.ViewHolder, position: Int) {
        val dataProduks = dataProduk[position]

        holder.imageProduk?.let { image->
            context?.let { context ->
                Glide.with(context)
                    .load(dataProduks.photo)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.ic_baseline_image_not_supported_24)
                    .into(image)
            }
        }

        holder.textFarmer?.text = dataProduks.farmer
        holder.tvNamaProduk?.text = dataProduks.title
        holder.tvHargaProduk?.text = NumberFormat.getInstance().format(dataProduks.sell_price)
        holder.tvUnitProduk?.text = dataProduks.unit
        holder.tvGradeProduk?.text = dataProduks.grade
        holder.tvStokProduk?.text = "Stok " + dataProduks.stock.toString()

        holder.clProduk?.setOnClickListener {
            val intent = Intent(context, DetailProdukActivity::class.java)

            intent.putExtra("dataProduks",dataProduks)

            if(position < 10){
                intent.putExtra("rekomended", 3)
            }else{
                intent.putExtra("rekomended", 1)

            }
//            intent.putExtra("")
//            intent.putExtra("dataProdukPage", dataProdukIntent)
            context?.startActivity(intent)

        }

        holder.btnBeliProduk?.setOnClickListener {
            if(db?.keranjangPembelianPopulerDao?.isProductExist(dataProduks.id_harvest) == true){
                db?.keranjangPembelianPopulerDao?.updateQuantityPlus(dataProduks.id_harvest)
                Toast.makeText(context, "Berhasil Menambah Jumlah Barang", Toast.LENGTH_SHORT).show()


            }else{
                dataProduks.quantity = 1
                db?.keranjangPembelianPopulerDao?.add(dataProduks)
                Toast.makeText(context, "Berhasil Dimasukkan Keranjang", Toast.LENGTH_SHORT).show()
            }

        }
    }

    override fun getItemCount(): Int {
         return  dataProduk.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageProduk: ImageView? = itemView.findViewById(R.id.iv_produk)
        val textFarmer: TextView? = itemView.findViewById(R.id.tv_farmer)
        val tvNamaProduk: TextView? = itemView.findViewById(R.id.tv_nama_produk)
        val tvHargaProduk: TextView? = itemView.findViewById(R.id.tv_harga_produk)
        val tvUnitProduk: TextView? = itemView.findViewById(R.id.tv_unit_produk)
        val tvGradeProduk: TextView? = itemView.findViewById(R.id.tv_grade_produk)
        val tvStokProduk: TextView? = itemView.findViewById(R.id.tv_stok_produk)
        val clProduk: ConstraintLayout? = itemView.findViewById(R.id.cl_produk)
        val btnBeliProduk: LinearLayout? = itemView.findViewById(R.id.btn_beli_produk)

    }
}