package com.example.kecipir.ui.webView

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import com.example.kecipir.R
import kotlinx.android.synthetic.main.activity_web.*

class WebActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        tv_title_produk.text = intent.getStringExtra("judul")
        wv_content.settings.setJavaScriptEnabled(true)

        wv_content.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                intent.getStringExtra("url")?.let { view?.loadUrl(it) }
                return true
            }
        }
        intent.getStringExtra("url")?.let { wv_content.loadUrl(it) }

        img_back.setOnClickListener {
            finish()
        }
    }
}