package com.example.kecipir.ui.detailProduk

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.kecipir.R
import com.example.kecipir.db.KeranjangPembelianDatabase
import com.example.kecipir.db.model.DataProducts
import com.example.kecipir.ui.detailProduk.Adapter.RekomendasiProdukAdapter
import com.example.kecipir.ui.keranjang.KeranjangActivity
import com.example.kecipir.utils.NetworkManager
import kotlinx.android.synthetic.main.activity_detail_produk.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.NumberFormat
import kotlin.random.Random

class DetailProdukActivity : AppCompatActivity() {
    var dataProducts: ArrayList<DataProducts> = ArrayList()
    private val detailProdukViewModel by viewModel<DetailProdukViewModel>()
    private var dataProduk: ArrayList<DataProducts> = ArrayList()
    private var shareLink : String? = ""
    private lateinit var db: KeranjangPembelianDatabase


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_produk)

        var dataSemua = intent.getParcelableExtra<DataProducts>("dataProduks")

       db= Room.databaseBuilder(
            this,
            KeranjangPembelianDatabase::class.java, "KeranjangPembelian"
        ).allowMainThreadQueries().build()

        tv_title_produk.text = dataSemua?.title.toString()
        tv_name_produk_detail.text = dataSemua?.title.toString()
        tv_harga_detail.text = "Rp. " + NumberFormat.getInstance().format(dataSemua?.sell_price)
        tv_unit_detail.text= "/ " +dataSemua?.unit.toString()
        tv_stok_detail.text = "stok " + dataSemua?.stock.toString()
        tv_petani_detail.text = dataSemua?.farmer.toString()
        tv_grade_detail.text = dataSemua?.grade
        shareLink = dataSemua?.share_link

        dataSemua?.photo?.let { Log.d("gambaropo", it) }
        Glide.with(this)
            .load(dataSemua?.photo)
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .placeholder(R.drawable.ic_baseline_image_not_supported_24)
            .into(iv_detail_product)

        detailProdukViewModel.produkList.observe(this, Observer { response->
            if(response!=null){
                for(i in 0..4){

                    dataProduk.add(response.data[Random.nextInt(0,(response.data.size-1))])
                }
                rv_produk_detail.adapter = RekomendasiProdukAdapter(DetailProdukActivity@this, dataProduk)
                rv_produk_detail.layoutManager = LinearLayoutManager(DetailProdukActivity@this, LinearLayoutManager.HORIZONTAL, false)
            }
        })

        if(NetworkManager.isOnline(this)) {
            detailProdukViewModel.getProduk()
        }else{

            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_LONG).show()

        }


        img_share.setOnClickListener {
            val i= Intent()
            i.action=Intent.ACTION_SEND
            i.putExtra(Intent.EXTRA_TEXT,"Ayo segera belanja " + tv_title_produk.text.toString()+ " hanya di Kecipir. Klik " +  shareLink  )
            i.type="text/plain"
            startActivity(Intent.createChooser(i,"Share To:"))
        }
        img_back.setOnClickListener {
            finish()
        }

        btn_beli_produk.setOnClickListener {
            if(dataSemua?.id_harvest?.let { it1 ->
                    db?.keranjangPembelianPopulerDao?.isProductExist(
                        it1
                    )
                } == true){
                db?.keranjangPembelianPopulerDao?.updateQuantityPlus(dataSemua?.id_harvest)
                Toast.makeText(this, "Berhasil Menambah Jumlah Barang", Toast.LENGTH_SHORT).show()


            }else{
                dataSemua?.quantity = 1
                dataSemua?.let { it1 -> db?.keranjangPembelianPopulerDao?.add(it1) }
                Toast.makeText(this, "Berhasil Dimasukkan Keranjang", Toast.LENGTH_SHORT).show()

            }
        }

        img_shopping_basket.setOnClickListener {
            startActivity(Intent(this, KeranjangActivity::class.java))
        }

    }
}