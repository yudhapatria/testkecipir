package com.example.kecipir.ui.keranjang

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.GridLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.kecipir.R
import com.example.kecipir.db.KeranjangPembelianDatabase
import com.example.kecipir.db.model.DataProducts
import kotlinx.android.synthetic.main.activity_keranjang.*
import java.text.NumberFormat

class KeranjangActivity : AppCompatActivity() {
    var dataProduk : ArrayList<DataProducts> = ArrayList()
    var totalHarga : Int = 0
    private lateinit var adapter: KeranjangAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_keranjang)

        var  db = Room.databaseBuilder(
                this,
                KeranjangPembelianDatabase::class.java, "KeranjangPembelian"
            ).allowMainThreadQueries().build()

      dataProduk.addAll(db?.keranjangPembelianPopulerDao?.findAllProduct())
        adapter = KeranjangAdapter(this, dataProduk)
      rv_keranjang.adapter = adapter
      rv_keranjang.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        ll_belanja_lagi.setOnClickListener {
            finish()
        }

        for(items in dataProduk){
            totalHarga += items.promo_price * items.quantity
        }

        tv_total_harga.text = "Rp. " + NumberFormat.getInstance().format(totalHarga)

        adapter.onItemClick = {pos ->

            dataProduk.clear()
            totalHarga = 0

            dataProduk.addAll(db?.keranjangPembelianPopulerDao?.findAllProduct())

            for(items in dataProduk){
                totalHarga += items.promo_price * items.quantity
            }

            tv_total_harga.text = "Rp. " + NumberFormat.getInstance().format(totalHarga)
        }


        img_back.setOnClickListener {
            finish()
        }
    }
}