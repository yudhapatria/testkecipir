package com.example.kecipir.ui.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.kecipir.R
import com.example.kecipir.db.KeranjangPembelianDatabase
import com.example.kecipir.db.model.DataCategory
import com.example.kecipir.db.model.DataPage
import com.example.kecipir.db.model.DataProducts
import com.example.kecipir.ui.home.adapter.CategoryAdapter
import com.example.kecipir.ui.home.adapter.ProdukAdapter
import com.example.kecipir.ui.home.adapter.PromoAdapter
import com.example.kecipir.ui.keranjang.KeranjangActivity
import com.example.kecipir.ui.searchProduk.SearchProdukActivity
import com.example.kecipir.ui.webView.WebActivity
import com.example.kecipir.utils.EndlessRecyclerOnScrollListener
import com.example.kecipir.utils.NetworkManager
import com.example.kecipir.utils.noNetworkConnectivityError
import com.jama.carouselview.enums.IndicatorAnimationType
import com.jama.carouselview.enums.OffsetType
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.koin.android.viewmodel.ext.android.viewModel
class HomeFragment : Fragment() {

    private val homeViewModel by viewModel<HomeViewModel>()

    private lateinit var categoryAdapter : CategoryAdapter
    private var dataCategory : ArrayList<DataCategory> =    ArrayList()
    private lateinit var gridLayoutManger : GridLayoutManager

    private var dataProduk: ArrayList<DataProducts> = ArrayList()
    private var pagination = 1
    private lateinit var produkAdapter: ProdukAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var scrollListenerPopular: EndlessRecyclerOnScrollListener
    private var stopPagination = false

    private var dataProdukPromo : ArrayList<DataProducts> =    ArrayList()
    private var dataProdukPagination: ArrayList<DataProducts> = ArrayList()

    private var totalData : Int = 0
    private var indexData : Int = 0
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)




        produkAdapter = ProdukAdapter(requireContext(), dataProdukPagination)
        linearLayoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        root.rv_semua_produk.adapter = produkAdapter
        root.rv_semua_produk.layoutManager = linearLayoutManager

//        scrollListenerPopular = object : EndlessRecyclerOnScrollListener(linearLayoutManager){
//            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
//
//                if(dataProduk.size > dataProdukPagination.size){
//                    totalData = dataProduk.size - dataProdukPagination.size
//                    if(totalData > 9){
//                        for(i in indexData+1..indexData+9){
//                            indexData = i
//                            dataProdukPagination.add(dataProduk[indexData])
//                        }
//                    }else if (totalData <= 0){
//                        for(i in indexData+1..(indexData+totalData-1)){
//                            indexData = i
//                            dataProdukPagination.add(dataProduk[indexData])
//                        }
//                    }
//                }
//
//                produkAdapter.notifyDataSetChanged()
//
//            }
//
//        }

//        root.rv_semua_produk.addOnScrollListener(scrollListenerPopular)

        homeViewModel.bannerList.observe(requireActivity(), Observer {response->
                if(response!=null){
                    carousel_view.apply {
                        size = response.data.size
                        resource = R.layout.image_carousel_item
                        autoPlay = true
                        carouselOffset = OffsetType.CENTER
                        setCarouselViewListener { view, position ->
                            // Example here is setting up a full image carousel
                            val imageView = view.findViewById<ImageView>(R.id.iv_carousel)
                            imageView?.let { imageView->
                                requireContext().let { context->
                                    Glide.with(context)
                                            .load(response.data[position].image.toString())
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .placeholder(R.drawable.ic_baseline_image_not_supported_24)
                                            .into(imageView)

                                }
                            }
                           view.setOnClickListener {
                                var i = Intent(requireContext(), WebActivity::class.java)
                               i.putExtra("url", response.data[position].url)
                               i.putExtra("judul", response.data[position].judul)
                               startActivity(i)
                           }
                        }
                        // After you finish setting up, show the CarouselView
                        show()
                    }
                }
        })


        homeViewModel.categoryList.observe(requireActivity(), Observer { responseCategory->
            if(responseCategory !=null){
                dataCategory.addAll(responseCategory.data)
                categoryAdapter = CategoryAdapter(requireContext(), dataCategory)
                gridLayoutManger = GridLayoutManager(requireContext(), 2, GridLayoutManager.HORIZONTAL, false)
                rv_category.layoutManager = gridLayoutManger
                rv_category.adapter = categoryAdapter
            }
        })

        homeViewModel.produkPromoList.observe(requireActivity(), Observer { responseProduk->
            if(responseProduk!=null){

                for (items in responseProduk.data){
                    if(items.discount != "0%"){
                        dataProdukPromo.add(items)
                    }else{
                        dataProduk.add(items)
                    }


                }

                rv_promo.adapter= PromoAdapter(requireContext(), dataProdukPromo)
                rv_promo.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)


                if(dataProduk.size > 9){
                    for(i in 0..9) {
                        dataProdukPagination.add(dataProduk[i])
                    }
                    indexData  = 9

                }
                produkAdapter.notifyDataSetChanged()

            }

            root.pg_home.visibility = View.GONE
            root.ll_home.visibility = View.VISIBLE
        })





        homeViewModel.showLoading.set(true)
        if(NetworkManager.isOnline(requireContext())) {
            homeViewModel.getBanner()
            homeViewModel.getCategory()
            homeViewModel.getProdukPromo()
        }else{
            Toast.makeText(requireContext(), "No Internet Connection", Toast.LENGTH_LONG).show()
        }
        root.cl_searchSayur.setOnClickListener {
            requireContext().startActivity(Intent(context, SearchProdukActivity::class.java))
        }

        root.txt_lainnya_semua_produk.setOnClickListener {
            var i = Intent(requireContext(), SearchProdukActivity::class.java)
            i.putExtra("produk", 1)
            startActivity(i)
        }

        root.txt_lainnya_produk_promo.setOnClickListener {
            var i = Intent(requireContext(), SearchProdukActivity::class.java)
            i.putExtra("produk", 2)
            startActivity(i)
        }


        root.img_shopping_basket.setOnClickListener {
            startActivity(Intent(requireContext(), KeranjangActivity::class.java))
        }

        return root
    }
}