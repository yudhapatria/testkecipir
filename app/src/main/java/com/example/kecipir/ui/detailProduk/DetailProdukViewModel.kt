package com.example.kecipir.ui.detailProduk

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.kecipir.db.model.PageResponse
import com.example.kecipir.db.model.ProductsResponse
import com.example.kecipir.repository.HomeRepository
import com.example.kecipir.ui.baseViewModel.BaseViewModel
import com.example.kecipir.utils.AppResult
import com.example.kecipir.utils.SingleLiveEvent
import kotlinx.coroutines.launch

class DetailProdukViewModel constructor(private val repository: HomeRepository) : BaseViewModel() {

    val produkList = MutableLiveData<ProductsResponse>()
    val showLoading = ObservableBoolean()
    val showError= SingleLiveEvent<String>()

    fun getProduk(){
        showLoading.set(true)
        viewModelScope.launch {
            val result =repository.getAllProduct()
            showLoading.set(false)

            when(result){
                is AppResult.Success->{

                    result.successData?.let {
                        produkList.value = it
                    }
                }
                is AppResult.Error -> showError.value = result.exception.message

            }
        }
    }

}